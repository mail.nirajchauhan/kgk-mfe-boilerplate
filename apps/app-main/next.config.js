/** @type {import('next').NextConfig} */
// const nextConfig = {
//   reactStrictMode: true,
// }

// module.exports = nextConfig

// next.config.js
// either from default
const NextFederationPlugin = require('@module-federation/nextjs-mf');

module.exports = {
  async rewrites() {
    return [
      {
        source: '/about',
        destination: '/',
      },
    ]
  },
  reactStrictMode: true,
  webpack(config, options) {
    const { isServer } = options;
    config.plugins.push(
      new NextFederationPlugin({
        name: 'shell-app',
        remotes: {
          dashboard: `dashboard@http://localhost:3006/remoteEntry.js`,
        },
        filename: 'shell.js',
        exposes: {
          // 
        },
        shared: {
          // whatever else
        },
        extraOptions: { 
          debug: false
        }
      })
    );

    return config;
  },
};
import logo from './logo.svg'
import './App.css'
// import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <div>
        <h1>Home Page</h1>
        <br/>
        Click to navigate <Link to="about">About Us</Link>
      </div>
    ),
  },
  {
    path: "about",
    element: (
      <div>
        <h1>About Us Page</h1>
        <br/>
        Click to navigate <Link to="/"> Home Page</Link>
      </div>
    ),
  },
])


function App() {
  return (
    <div className="App">
      {/* <h1>Test Log Component</h1> */}
      <RouterProvider router={router} />
    </div>
  )
}

export default App

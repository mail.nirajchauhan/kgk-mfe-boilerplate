const deps = require("./package.json").dependencies;
const { ModuleFederationPlugin } = require("webpack").container;

module.exports = () => ({
  webpack: {
    plugins: {
      add: [
        new ModuleFederationPlugin({
          name: "dashboard",
          filename:"remoteEntry.js",
          // library:{
          //   type: 'var',
          //   name: 'dashboard',
          // },
          exposes: {
              // expose each component
              "./remoteComponent": "./src/App.js"
          },
          shared: {
            ...deps,
            react: {
              eager: true,
              singleton: true,
            },
            "react-dom": {
              eager: true,
              singleton: true,
            },
            "react-router-dom": {
              eager: true,
              singleton: true,
            },
          },
        }),
      ],
    },
    configure: (webpackConfig) => ({
      // experiments: {
      //   outputModule: true 
      // },
      ...webpackConfig,
      output: {
        ...webpackConfig.output,
        publicPath: "auto",
        // scriptType: 'text/javascript'
        // library: {
        //   type: "module"
        // },
      },
    }),
  }, 
});